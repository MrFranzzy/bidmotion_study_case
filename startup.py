import click
import json
import re
from urllib.request import urlopen
from urllib.error import URLError

from bs4 import BeautifulSoup


def _get_domain(url):
    url_partition = url.partition("://")
    suffix_partition = url_partition[2].partition("/")
    return (
        url_partition[0] + url_partition[1] + suffix_partition[0],
        suffix_partition[1] + suffix_partition[2],
    )


def _format_link_output(url, link_href):
    if re.match("^https?://", link_href) is not None:
        return link_href
    if link_href[0] == "/":
        return _get_domain(url)[0] + link_href
    return url + "/" + link_href if url[-1] != "/" else url + link_href


def _handle_stdout(urls):
    for url in urls:
        print("\n******* %s\n" % url)
        try:
            html_page = urlopen(url)
            soup = BeautifulSoup(html_page, "html.parser")
            for link in soup.findAll("a"):
                link_href = link.get("href")
                if link_href:
                    print(_format_link_output(url, link_href))
        except URLError:
            print("Url not found")


def _handle_json(urls):
    output_json = {}
    for url in urls:
        print("******* Parsing %s" % url)
        try:
            html_page = urlopen(url)
            soup = BeautifulSoup(html_page, "html.parser")
            for link in soup.findAll("a"):
                link_href = link.get("href")
                if link_href:
                    clean_url = _format_link_output(url, link_href)
                    domain, relative_path = _get_domain(clean_url)
                    if clean_url in output_json.keys():
                        output_json[clean_url].append(relative_path)
                    else:
                        output_json.update({clean_url: [relative_path]})
        except URLError:
            print("Url not found")
    print(json.dumps(output_json, sort_keys=True, indent=4))


_FORMAT_OPTIONS = {"stdout": _handle_stdout, "json": _handle_json}


@click.command()
@click.argument("urls", nargs=-1)
@click.option(
    "-o",
    "--format_option",
    default="stdout",
    help='fomrat option, either "stdout" (default) or "json"',
    type=click.Choice(_FORMAT_OPTIONS.keys()),
)
def url_link_extract(urls, format_option):
    _FORMAT_OPTIONS[format_option](urls)


if __name__ == "__main__":
    url_link_extract()
