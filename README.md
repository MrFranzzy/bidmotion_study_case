# BidMotion study case

## Prerequisites

* Python (v3.7.0)
* Pipenv (tested w/ version 2018.10.13)

## Getting started

```bash
git clone https://MrFranzzy@bitbucket.org/MrFranzzy/bidmotion_study_case.git
cd bidmotion_study_case
pipenv install
pipenv run dev
```

## Exercise "Ops"

Using the Dockerfile in this project as well as a service file and docker-compose file such as:
```
[Unit]
Description=user_case
After=docker.service
BindsTo=docker.service
Conflicts=shutdown.target reboot.target halt.target

[Service]
Restart=always
WorkingDirectory=/opt/user_case
TimeoutSec=0
ExecStart=/usr/bin/docker-compose up
ExecStop=/usr/bin/docker-compose down

[Install]
WantedBy=multi-user.target
```
and
```
version: "3"

services:
  opapi:
    container_name: user_case
    image: my_registry/user_case:version
    restart: unless-stopped
```

A configuration management tool would run the application in a Docker container managed by a linux service. In the pipeline of this project we would want to upload to our registry the (new) image of this app.

## Exercise "DevOps"

I would use Kubernetes, if done properly scaling should not be an issue.
