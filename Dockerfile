# base image
FROM python:3.7.0-alpine

# set working directory
RUN mkdir -p /opt/study_case
WORKDIR /opt/study_case

# copy just enough for installation
COPY Pipfile .
COPY Pipfile.lock .

# install everything
RUN pip install pipenv && pipenv install

# copy the rest
COPY . .

# run application
CMD ["pipenv", "run", "dev"]
